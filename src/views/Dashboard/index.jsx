import React, { Component } from 'react';

// Externals
import PropTypes from 'prop-types';

// Material helpers
import { withStyles } from '@material-ui/core';

import styled from 'styled-components';
// Material components
import { Grid, Typography, Paper } from '@material-ui/core';

// Shared layouts
import { Dashboard as DashboardLayout } from 'layouts';

// Custom components
import {
  Budget,
  Users,
  Progress,
  UserCard,
  RecentContacts,
  Profit,
  SalesChart,
  DevicesChart,
  ProductList,
  OrdersTable, TopCard
} from './components';


const HeaderWrapper = styled.div`
  padding-top:20px;
  padding-bottom:20px;
`;

const TopMovedContent1 = styled.div`
  margin-top: 0rem;
  height: 100%;
  
  @media(min-width: 1280px) {
    margin-top: -3rem;
  }
`

const TopMovedContent2 = styled.div`
  margin-top: 0rem;
  height: 100%;

  @media(min-width: 1280px) {
    margin-top: -6rem;
  }
`
// Component styles
const styles = theme => ({
  root: {
    padding: '2rem'
  },
  userCardRoot: {
    padding: theme.spacing.unit * 2
  },
  item: {
    height: '100%'
  },
  messageRoot: {
    height: '100%',
    padding: '2rem',
    background: '#1665D8',
    border: '1px solid #E4E7EB',
    borderRadius: '4px',
  },
  dashboardTitle: {
    fontWeight: '500',
    fontSize: '1.9rem',
    lineHeight: '28px',
    color: '#3A3B3F'
  },
  title1: {
    color: theme.palette.primary.contrastText,
    marginBottom: '1rem',
    fontSize: '1rem',
    fontWeight: '500',
    lineHeight:'1rem'
  },
  title2: {
    color: theme.palette.primary.contrastText,
    marginBottom: '2rem',
    fontSize: '1.5rem',
    fontWeight: '500',
    lineHeight:'2rem'
  },
  title3: {
    color: theme.palette.primary.contrastText,
    fontWeight: '300',
    fontSize: '1rem',
    lineHeight:'1rem'
  },
  matchRoot: {
    height: '100%',
    padding: '2rem',
    background: '#45B880',
    border: '1px solid #E4E7EB',
    borderRadius: '4px'
  },
  invitationRoot: {
    height: '100%',
    padding: '2rem',
    background: '#EF5350',
    borderRadius: '4px'
  },
  accountRoot: {
    height: '100%',
    padding: '2rem',
    background: 'linear-gradient(305deg, rgba(2,9,186,1), rgba(39,181,255,1) 70.31%)',
    borderRadius: '4px'
  },
  discountCard: {
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  customGrid: {
    paddingRight: '0px !important'
  }

});

class Dashboard extends Component {
  render() {
    const { classes } = this.props;


    return (
      <DashboardLayout title="Dashboard">
        <div className={classes.root}>
          <Grid
            container
            spacing={2}
          >
            <Grid
              item
              lg={3}
              sm={6}
              xl={3}
              xs={6}
            >
              <TopCard
                icon="1"
                root={classes.messageRoot}
                text1="Messages"
                text2="10 New Messages"
                text3="From Sachini & 13 others"
                title1={classes.title1}
                title2={classes.title2}
                title3={classes.title3}
              />
            </Grid>

            <Grid
              item
              lg={3}
              sm={6}
              xl={3}
              xs={6}
            >
              <TopCard
                icon="2"
                root={classes.matchRoot}
                text1="Matches"
                text2="3 New Matches"
                text3="100 Matches this month"
                title1={classes.title1}
                title2={classes.title2}
                title3={classes.title3}
                upIcon="true"
              />
            </Grid>

            <Grid
              item
              lg={3}
              sm={6}
              xl={3}
              xs={6}
            >
              <TopCard
                icon="3"
                root={classes.invitationRoot}
                text1="Invitations"
                text2="5 Pending"
                text3="20 Invitations this month"
                title1={classes.title1}
                title2={classes.title2}
                title3={classes.title3}
                upIcon="true"
              />
            </Grid>

            <Grid
              item
              lg={3}
              sm={6}
              xl={3}
              xs={6}
            >
              <TopCard
                icon="4"
                root={classes.accountRoot}
                text1="Account Status"
                text2="Standard Member"
                text3="Upgrade now to get new Features"
                title1={classes.title1}
                title2={classes.title2}
                title3={classes.title3}
              />
            </Grid>
          </Grid>











          <Grid
            item
            lg={12}
            sm={12}
            xl={12}
            xs={12}
          >
            <HeaderWrapper>
              <Typography
                className={classes.dashboardTitle}
                variant="h4"

              >
                Statistics
              </Typography>
            </HeaderWrapper>
          </Grid>

          <Grid
            container
            spacing={2}
          >
            <Grid
              item
              lg={3}
              sm={6}
              xl={3}
              xs={6}
            >
              <Progress className={classes.item} />

            </Grid>
            <Grid
              item
              lg={3}
              sm={6}
              xl={3}
              xs={6}
            >
              <Users className={classes.item} />
            </Grid>
            <Grid
              item
              lg={3}
              sm={6}
              xl={3}
              xs={6}
            >
              <Budget className={classes.item} />
            </Grid>
            <Grid
              item
              lg={3}
              sm={6}
              xl={3}
              xs={6}
            >
              <TopMovedContent1>
                <Paper className={classes.discountCard}>
                  <Typography
                    variant="h4"
                  >
                    Discounts
                  </Typography>
                </Paper>
              </TopMovedContent1>
            </Grid>
          </Grid>

          <Grid
            item
            lg={12}
            sm={12}
            xl={12}
            xs={12}
          >
            <HeaderWrapper>
              <Typography
                className={classes.dashboardTitle}
                variant="h4"
              >
                New Matches for you
              </Typography>
            </HeaderWrapper>
          </Grid>





          <Grid
            container
            justify="space-between"
            spacing={2}
          >

            <Grid
              className={classes.customGrid}
              container
              item
              justify="space-between"
              lg={9}
              sm={12}
              spacing={1}
              xl={9}
              xs={12}
            >
              <Grid
                item
                lg={6}
                sm={12}
                xl={6}
                xs={12}

              >
                <UserCard
                  className={classes.messageRoot}
                  name="Janaka, 28"
                  root={classes.userCardRoot}
                  text1="from Colombo"
                  text2="Buddhist, Sinhala"
                  text3="Undergraduate"
                />
              </Grid>

              <Grid
                item
                lg={6}
                sm={12}
                xl={6}
                xs={12}
              >
                <UserCard
                  className={classes.root}
                  name="Madhuranga, 28"
                  root={classes.userCardRoot}
                  text1="from Colombo"
                  text2="Buddhist, Sinhala"
                  text3="Undergraduate"
                />
              </Grid>
              <Grid
                item
                lg={6}
                sm={12}
                xl={6}
                xs={12}

              >
                <UserCard
                  className={classes.messageRoot}
                  name="Janaka, 28"
                  root={classes.userCardRoot}
                  text1="from Colombo"
                  text2="Buddhist, Sinhala"
                  text3="Undergraduate"
                />
              </Grid>

              <Grid
                item
                lg={6}
                sm={12}
                xl={6}
                xs={12}
              >
                <UserCard
                  className={classes.root}
                  name="Madhuranga, 28"
                  root={classes.userCardRoot}
                  text1="from Colombo"
                  text2="Buddhist, Sinhala"
                  text3="Undergraduate"
                />
              </Grid>


            </Grid>




            <Grid
              container
              item
              lg={3}
              sm={12}
              xl={3}
              xs={12}

            >
              <Grid
                item
                lg={12}
                sm={12}
                xl={12}
                xs={12}
              >
                <TopMovedContent2>
                  <RecentContacts />
                </TopMovedContent2>
              </Grid>
            </Grid>

          </Grid>
        </div>
      </DashboardLayout>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Dashboard);
