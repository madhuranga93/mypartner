import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { withStyles,Avatar,LinearProgress,Button } from '@material-ui/core';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import classNames from 'classnames';
import styles from './styles';
import {   Message as MessageIcon } from '@material-ui/icons';

const CardWrapper = styled.div`
  display:flex;
  flex-direction:row;
  justify-content: space-between;
  align-items: center;
  padding-bottom: 0.8rem;
  border-bottom: 1px solid #E4E7EB;
  padding-top: 1rem;
`;


class RecentContacts extends Component {

  contaCard1(){
    const { classes} = this.props;
    return ( <CardWrapper>

      <img
        alt="Roman Kutepov"
        className={classes.avatar}
        src="/images/avatars/avatar_1.png"
      />

      <div>
        <Typography
          className={classes.title2}
          variant="subtitle1"
        >
          Sachini Soyza
        </Typography>
        <Typography
          className={classes.online}
          variant="subtitle1"
        >
              Online
        </Typography>
       
      </div>
      <MessageIcon className={classes.icon} />
    </CardWrapper>
    )  }

  contaCard2(){
    const { classes} = this.props;
    return ( <CardWrapper>

      <img
        alt="Roman Kutepov"
        className={classes.avatar}
        src="/images/avatars/avatar_1.png"
      />

      <div>
        <Typography
          className={classes.title2}
          variant="subtitle1"
        >
         Anukshi Rodrigo
        </Typography>
        <Typography
          className={classes.title3}
          variant="subtitle1"
        >
              Online 4 hours ago
        </Typography>
       
      </div>
      <MessageIcon className={classes.icon} />
    </CardWrapper>
    )  }


  contaCard3(){
    const { classes} = this.props;
    return ( <CardWrapper>

      <img
        alt="Roman Kutepov"
        className={classes.avatar}
        src="/images/avatars/avatar_1.png"
      />

      <div>
        <Typography
          className={classes.title2}
          variant="subtitle1"
        >
          Dulani Perera
        </Typography>
        <Typography
          className={classes.title3}
          variant="subtitle1"
        >
              Online 10 hours ago
        </Typography>
       
      </div>
      <MessageIcon className={classes.icon} />
    </CardWrapper>
    )  }

  contaCard4(){
    const { classes} = this.props;
    return ( <CardWrapper>
  
      <img
        alt="Roman Kutepov"
        className={classes.avatar}
        src="/images/avatars/avatar_1.png"
      />
  
      <div>
        <Typography
          className={classes.title2}
          variant="subtitle1"
        >
            Nayani Karunarathne
        </Typography>
        <Typography
          className={classes.title3}
          variant="subtitle1"
        >
                Online 15 hours ago
        </Typography>
         
      </div>
      <MessageIcon className={classes.icon} />
    </CardWrapper>
    )  }

  render() {

    const { root ,title1,title2,title3,text1,text2,text3,classes} = this.props;
    const rootClassName = classNames(root);


    return (
  
      <Paper className={classes.root}>
        <Typography
          className={classes.title1}
          variant="h4"
        >
              Recent Contacts
        </Typography>
        {this.contaCard1()}
        {this.contaCard2()}
        {this.contaCard3()}
        {this.contaCard4()}
      </Paper>
   
    );
  }
}

RecentContacts.propTypes = {
  classes: PropTypes.object.isRequired,
  root: PropTypes.object.isRequired,
  title1: PropTypes.object.isRequired,
  title2: PropTypes.object.isRequired,
  title3: PropTypes.object.isRequired
};

export default withStyles(styles)(RecentContacts);