export default theme => ({
  root: {
    padding: theme.spacing.unit * 3,
    display: 'flex',
    flexDirection: 'column'
  },
  button1:{
    paddingLeft:'3rem',
    paddingRight:'3rem',
    borderColor:'#EF5350',
    color:'#EF5350',
    fontSize: '1rem',
    borderRadius: '4px'
  },
  button2:{
    paddingLeft:'3rem',
    paddingRight:'3rem',
    borderColor:'#45B880',
    color:'#45B880',
    fontSize: '1rem',
    borderRadius: '4px'
  },
  content: {
    alignItems: 'center',
    display: 'flex'
  },
  title1: {
    color: '#3A3B3F',
    fontWeight: 500,
    fontSize:'1.3rem',
    paddingBottom:'2rem'
  },
  title2: {
    color: '#3A3B3F',
    fontWeight: 500,
    lineHeight:'1.2rem',
    fontSize: '1rem'
  },
  title3: {
    color: 'rgba(0, 0, 0, 0.38)',
    lineHeight:'1.2rem',
    fontWeight: 'normal',
    fontSize: '0.8rem'
  },
  online:{
    color:'	#32CD32',
    lineHeight:'1.2rem',
    fontWeight: 'normal',
    fontSize: '0.8rem'

  },
  value: {
    marginTop: theme.spacing.unit
  },
  iconWrapper: {
    alignItems: 'center',
    backgroundColor: theme.palette.danger.main,
    borderRadius: '50%',
    display: 'inline-flex',
    height: '4rem',
    justifyContent: 'center',
    marginLeft: 'auto',
    width: '4rem'
  },
  icon: {
    color: '#4784F2',
    fontSize: '2rem',
    height: '2rem',
    width: '2rem'
  },
  footer: {
    marginTop: theme.spacing.unit * 2,
    display: 'flex',
    alignItems: 'center'
  },
  difference: {
    alignItems: 'center',
    color: theme.palette.danger.dark,
    display: 'inline-flex',
    fontWeight: 700
  },
  caption: {
    marginLeft: theme.spacing.unit
  },
  avatar: {
    width: '60px',
    height: 'auto'
  },
});
      