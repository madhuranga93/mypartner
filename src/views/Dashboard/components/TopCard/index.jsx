import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import classNames from 'classnames';
import styles from './styles';
import { Message as MessageIcon, People as PeopleIcon, InsertInvitation as InsertInvitationIcon, AccountBalanceWallet as ABWallet, ArrowDropUp as UpIcon } from '@material-ui/icons';
const MainWrapper = styled.div`
  display:flex;
  flex-direction: row;
  height: 100%;
`;

const FirstRow = styled.div`
  flex:1;
  flex-direction:column;
  display:flex;
`;

const BottomRow = styled.div`
  flex-direction:row;
  display:flex;
  margin-left: -0.5rem;
  align-items: flex-end;
`;

const StretchRow = styled.div`
  flex:1;
`;




class TopCard extends Component {


  SelectedIcon(icon){
    const { classes} = this.props;

    if(icon == '1')
      return(
        <MessageIcon className={classes.icon} />
      )
    if(icon == '2')
      return(
        <PeopleIcon className={classes.icon} />
      ) 
    if(icon == '3')
      return(
        <InsertInvitationIcon className={classes.icon} />
      ) 
    if(icon == '4')
      return(
        <ABWallet className={classes.icon} />
      )
  }

  render() {

    const { root ,title1,title2,title3,text1,text2,text3,classes,icon,upIcon} = this.props;
    const rootClassName = classNames(root);
    const rootTitle1 = classNames(title1);
    const rootTitle2 = classNames(title2);
    const rootTitle3 = classNames(title3);

    return (
  
      <Paper className={rootClassName}>
        <MainWrapper>
          <FirstRow>
            <Typography
              className={rootTitle1}
               
              variant="subtitle2"
            >
              {text1}
            </Typography>
            <StretchRow>
              <Typography
                className={rootTitle2}
                variant="h3"
              
              >
                {text2}
              </Typography>
            </StretchRow>
            <BottomRow>
              {upIcon === 'true' &&  <UpIcon className={classes.icon2}/> }
             
              <Typography
                className={rootTitle3}
                variant="subtitle1"
              >
                {text3}
              </Typography>
            </BottomRow>
          </FirstRow>
          <div>
            {this.SelectedIcon(icon)}
          </div>
           
        </MainWrapper>
      </Paper>
   
    );
  }
}

TopCard.propTypes = {
  classes: PropTypes.object.isRequired,
  root: PropTypes.object.isRequired,
  title1: PropTypes.object.isRequired,
  title2: PropTypes.object.isRequired,
  title3: PropTypes.object.isRequired
};

export default withStyles(styles)(TopCard);