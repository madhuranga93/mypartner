export default theme => ({
  root: {
    padding: '2rem'
  },
  content: {
    display: 'flex',
    alignItems: 'center'
  },
  title1: {
    color:'#66788A',
    fontSize: '1rem',
    fontWeight: '500'
  },
  details: {
    color:'#66788A',
    fontSize: '1rem',
    fontWeight: '500'
  },
  title: {
    color: theme.palette.text.secondary,
    fontWeight: 700
  },
  value: {
    marginTop: theme.spacing.unit,
    fontSize:'1.2rem'
  },
  iconWrapper: {
    alignItems: 'center',
    backgroundColor: theme.palette.primary.main,
    borderRadius: '50%',
    display: 'inline-flex',
    height: '4rem',
    justifyContent: 'center',
    marginLeft: 'auto',
    width: '4rem'
  },
  icon: {
    color: theme.palette.common.white,
    fontSize: '2rem',
    height: '2rem',
    width: '2rem'
  },
  footer: {
    marginTop: theme.spacing.unit * 3
  }
});
