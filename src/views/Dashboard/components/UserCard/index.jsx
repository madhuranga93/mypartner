import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { withStyles, Avatar, LinearProgress, Button } from '@material-ui/core';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import classNames from 'classnames';
import styles from './styles';
import { AttachMoney as AttachMoneyIcon } from '@material-ui/icons';
const SubWrapper = styled.div`
  display:flex;
  flex-direction: row;
  height: 100%;
`;

const FirstRow = styled.div`
  flex:1;
  flex-direction:column;
  display:flex;
`;

const ButtonContent = styled.div`
  display:flex;
  justify-content:space-around;
  align-items:center;
  border-top: 1px solid #E4E7EB;
  padding-top:0.8rem;
  margin-top:1rem;
`;





class UserCard extends Component {

  render() {

    const { root, name, text1, text2, text3, classes } = this.props;
    const rootClassName = classNames(root);


    return (

      <Paper className={rootClassName}>
        <div>
          <SubWrapper>
            <FirstRow>
              <Typography
                className={classes.title1}
                variant="h4"
              >

                {name}
              </Typography>

              <Typography
                className={classes.title2}
                variant="subtitle1"

              >
                {text1}
              </Typography>

              <Typography
                className={classes.title2}
                variant="subtitle1"

              >
                {text2}
              </Typography>

              <Typography
                className={classes.title2}
                variant="subtitle1"

              >
                {text3}
              </Typography>

            </FirstRow>
            <div>
              <Avatar
                alt="Roman Kutepov"
                className={classes.avatar}
                src="/images/avatars/avatar_1.png"
              />
            </div>
          </SubWrapper>
          <Typography
            className={classes.title3}
            variant="subtitle1"
          >
            100% Compatible Interests
          </Typography>
          <LinearProgress
            value={100}
            variant="determinate"
          />
          <ButtonContent>
            <Button
              className={classes.button1}
              color="#6202EE"
              variant="outlined"
            >
              Not Interested
            </Button>

            <Button
              className={classes.button2}
              color="#6202EE"
              variant="outlined"
            >
              View Profile
            </Button>
          </ButtonContent>
        </div>
      </Paper>

    );
  }
}

UserCard.propTypes = {
  classes: PropTypes.object.isRequired,
  root: PropTypes.object.isRequired,
  title1: PropTypes.object.isRequired,
  title2: PropTypes.object.isRequired,
  title3: PropTypes.object.isRequired
};

export default withStyles(styles)(UserCard);