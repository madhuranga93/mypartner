export default theme => ({
  root: {
    padding: theme.spacing.unit * 3
  },
  button1:{
    paddingLeft:'3rem',
    paddingRight:'3rem',
    borderColor:'#EF5350',
    color:'#EF5350',
    fontSize: '1rem',
    borderRadius: '4px'
  },
  button2:{
    paddingLeft:'3rem',
    paddingRight:'3rem',
    borderColor:'#45B880',
    color:'#45B880',
    fontSize: '1rem',
    borderRadius: '4px'
  },
  content: {
    alignItems: 'center',
    display: 'flex'
  },
  title1: {
    color: '#212529',
    fontWeight: 500,
    marginBottom:'20px',
    fontSize:'1.5rem'
  },
  title2: {
    color: 'rgba(0, 0, 0, 0.6)',
    fontWeight: 'normal',
    fontSize: '1rem'
  },
  title3: {
    color: '#002F33',
    marginTop: '10px',
    marginBottom:'10px',
    fontWeight: 'normal',
    fontSize: '1.2rem'
  },
  value: {
    marginTop: theme.spacing.unit
  },
  iconWrapper: {
    alignItems: 'center',
    backgroundColor: theme.palette.danger.main,
    borderRadius: '50%',
    display: 'inline-flex',
    height: '4rem',
    justifyContent: 'center',
    marginLeft: 'auto',
    width: '4rem'
  },
  icon: {
    color: theme.palette.common.white,
    fontSize: '2rem',
    height: '2rem',
    width: '2rem'
  },
  footer: {
    marginTop: theme.spacing.unit * 2,
    display: 'flex',
    alignItems: 'center'
  },
  difference: {
    alignItems: 'center',
    color: theme.palette.danger.dark,
    display: 'inline-flex',
    fontWeight: 700
  },
  caption: {
    marginLeft: theme.spacing.unit
  },
  avatar: {
    width: '124px',
    height: 'auto'
  },
});
    