export default theme => ({
  root: {
    padding: '2rem'
  },
  content: {
    alignItems: 'center',
    display: 'flex'
  },
  title: {
    color:'#66788A',
    fontSize: '1rem',
    fontWeight: '500'
  },
  value: {
    marginTop: theme.spacing.unit,
    color:'#212529',
    fontSize: '1.2rem',
    fontWeight: '500'
  },
  iconWrapper: {
    alignItems: 'center',
    backgroundColor: theme.palette.success.main,
    borderRadius: '50%',
    display: 'inline-flex',
    height: '4rem',
    justifyContent: 'center',
    marginLeft: 'auto',
    width: '4rem'
  },
  icon: {
    color: theme.palette.common.white,
    fontSize: '2rem',
    height: '2rem',
    width: '2rem'
  },
  footer: {
    marginTop: theme.spacing.unit * 2,
    display: 'flex',
    alignItems: 'center'
  },
  difference: {
    alignItems: 'center',
    color: theme.palette.success.dark,
    display: 'inline-flex',
    fontWeight: 700
  },
  caption: {
    marginLeft: theme.spacing.unit
  }
});
