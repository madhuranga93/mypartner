export default theme => ({
  root: {
    borderBottom: `1px solid ${theme.palette.border}`,
    backgroundColor: '#2196F3',
    display: 'flex',
    alignItems: 'center',
    height: '64px',
    zIndex: theme.zIndex.appBar
  },
  toolbar: {
    minHeight: 'auto',
    width: '100%'
  },
  title: {
    marginLeft: theme.spacing.unit,
    color:theme.palette.common.white,
   
  },
  menuButton: {
    marginLeft: '-4px'
  },
  menuIcon: {
    color:theme.palette.common.white,
  },
  notificationsButton: {
    marginLeft: 'auto',
    color:'white'
  },
  signOutButton: {
    marginLeft: theme.spacing.unit
  }
});
