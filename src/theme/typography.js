import palette from './palette';

export default {
  h1: {
    color: palette.text.primary,
    fontWeight: '500',
    fontSize: '3rem',
    letterSpacing: '-0.24px',
    lineHeight: '3rem'
  },
  h2: {
    color: palette.text.primary,
    fontWeight: '500',
    fontSize: '2.2rem',
    letterSpacing: '-0.24px',
    lineHeight: '1.8rem'
  },
  h3: {
    color: palette.text.primary,
    fontWeight: '500',
    fontSize: '2rem',
    letterSpacing: '-0.06px',
    lineHeight: '2rem'
  },
  h4: {
    color: palette.text.primary,
    fontWeight: '500',
    fontSize: '1.6rem',
    letterSpacing: '-0.06px',
    lineHeight: '2rem'
  },
  h5: {
    color: palette.text.primary,
    fontWeight: '500',
    fontSize: '1.2rem',
    letterSpacing: '-0.05px',
    lineHeight: '1.8rem'
  },
  h6: {
    color: palette.text.primary,
    fontWeight: '500',
    fontSize: '1.1rem',
    letterSpacing: '-0.05px',
    lineHeight: '1.8rem'
  },
  subtitle1: {
    color: palette.text.primary,
    fontSize: '1.2rem',
    letterSpacing: '-0.05px',
    lineHeight: '2rem'
  },
  subtitle2: {
    color: palette.text.primary,
    fontSize: '1.1rem',
    letterSpacing: 0,
    lineHeight: '1.2rem'
  },
  body1: {
    color: palette.text.primary,
    fontSize: '1.1rem',
    letterSpacing: '-0.05px',
    lineHeight: '2rem'
  },
  body2: {
    color: palette.text.primary,
    fontSize: '1rem',
    letterSpacing: '-0.04px',
    lineHeight: '1.1rem'
  },
  button: {
    color: palette.text.primary,
    fontSize: '1.1rem'
  },
  caption: {
    color: palette.text.secondary,
    fontSize: '1rem',
    letterSpacing: '0.3px',
    lineHeight: '1.2rem'
  },
};
